<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('critere_evaluations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('plancadre_id');
            $table->string('ennonce');
            $table->smallInteger('ponderation');
            $table->timestamps();

            $table->foreign('plancadre_id')->references('id')->on('plan_cadres')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('critere_evaluations');
    }
};
