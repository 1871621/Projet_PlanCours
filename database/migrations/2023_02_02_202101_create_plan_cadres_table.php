<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('plan_cadres', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('titre');
            $table->string('ponderation')->nullable();
            $table->float('unite')->nullable();
            $table->string('attitudes')->nullable();
            $table->string('intentions_educatives')->nullable();
            $table->text('intentions_pedagogiques')->nullable();
            $table->text('desc_cours')->nullable();
            $table->date('changement')->nullable();
            $table->date('approbation_depart')->nullable();
            $table->date('approbation_comite')->nullable();
            $table->integer('ponderation_final')->nullable();
            $table->foreignId("user_id")->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('plancadres_competences', function (Blueprint $table) {
            $table->foreignId('plancadres_id');
            $table->foreignId('competences_id');
            $table->boolean('complete')->nullable();
            $table->timestamps();

            $table->primary(['plancadres_id', 'competences_id']);
            $table->foreign('plancadres_id')->references('id')->on('plan_cadres')->onDelete('cascade');
            $table->foreign('competences_id')->references('id')->on('competences')->onDelete('cascade');
        });

        Schema::create('plancadres_prealable', function (Blueprint $table) {
            $table->foreignId('plan_cadre_id');
            $table->foreignId('prealable_id');
            $table->boolean('absolu')->nullable();
            $table->timestamps();

            $table->primary(['plan_cadre_id', 'prealable_id']);
            $table->foreign('plan_cadre_id')->references('id')->on('plan_cadres')->onDelete('cascade');
            $table->foreign('prealable_id')->references('id')->on('plan_cadres')->onDelete('cascade');
        });

        Schema::create('plancadres_corequi', function (Blueprint $table) {
            $table->foreignId('plan_cadre_id');
            $table->foreignId('corequi_id');
            $table->timestamps();

            $table->primary(['plan_cadre_id', 'corequi_id']);
            $table->foreign('plan_cadre_id')->references('id')->on('plan_cadres')->onDelete('cascade');
            $table->foreign('corequi_id')->references('id')->on('plan_cadres')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('plan_cadres');
    }
};
