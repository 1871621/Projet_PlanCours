<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('element_competences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('competence_id');
            $table->string('no');
            $table->string('texte');
            $table->text('contenu_local');
            $table->timestamps();

            $table->foreign('competence_id')->references('id')->on('competences')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('element_competences');
    }
};
