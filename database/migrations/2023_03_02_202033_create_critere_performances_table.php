<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('critere_performances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('elementcompetence_id');
            $table->string('no');
            $table->string('texte');
            $table->timestamps();

            $table->foreign('elementcompetence_id')->references('id')->on('element_competences')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('critere_performances');
    }
};
