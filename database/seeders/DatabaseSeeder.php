<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Competence;
use App\Models\CritereEvaluation;
use App\Models\CriterePerformance;
use App\Models\ElementCompetence;
use App\Models\PlanCadre;
use App\Models\Programme;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory(10)->create();
        PlanCadre::factory(5)
            ->has(Competence::factory()->count(2), 'competence')
            ->has(CritereEvaluation::factory()->count(2))
            ->has(Programme::factory())
            ->create();

        foreach (Competence::all() as $competence){
            $elementcompetences = ElementCompetence::factory()
                ->count(2)
                ->for($competence)
                ->create();
        }

        foreach (ElementCompetence::all() as $elementComp){
            $criterePerf = CriterePerformance::factory()
                ->for($elementComp)
                ->create();
        }

        DB::table('plan_cadres')->insert([
           'code'=>'420-4P2-HU',
           'titre'=>'Déceloppement WEB en PHP',
           'ponderation'=>'2-2-1',
           'unite'=>1.66,
           'intentions_educatives'=>"- développer l'autonomie et favoriser la polyvalence",
           'intentions_pedagogiques'=>"Ce cours de la 4e session doit être suivi en même temps, corequis, que le cours ASP.NET. Cest le 2e et dernier cours a traiter la competence 00ST",
            'desc_cours'=>"À la fin du cours, l'étudiant va être capable d'utiliser le framework Laravel",
            'user_id'=>1
        ]);



        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
