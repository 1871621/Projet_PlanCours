<?php

namespace Database\Factories;

use App\Models\Competence;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ElementCompetence>
 */
class ElementCompetenceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'no'=>fake()->numberBetween(1,10),
            'texte'=>fake()->text(),
            'contenu_local'=>fake()->text()
        ];
    }
}
