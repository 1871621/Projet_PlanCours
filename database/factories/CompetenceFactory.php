<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Competence>
 */
class CompetenceFactory extends Factory
{

    protected $table = 'competences';

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'code'=>fake()->randomNumber(5, false),
            'titre'=>fake()->company(),
            'enonce'=>fake()->company(),
            'annee_devis'=>fake()->randomNumber(1,31),
            'pages_devis'=>fake()->date(),
            'contexte'=>fake()->text(),
            'contexte_local'=>fake()->text()
        ];
    }
}
