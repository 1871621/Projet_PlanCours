<?php

namespace Database\Factories;

use App\Models\PlanCadre;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\CritereEvaluation>
 */
class CritereEvaluationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ennonce'=>fake()->text(),
            'ponderation'=>fake()->numberBetween(30,60),
            'plancadre_id'=> PlanCadre::factory()->createOne()->id
        ];
    }
}
