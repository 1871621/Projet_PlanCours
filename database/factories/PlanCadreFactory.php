<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PlanCadre>
 */
class PlanCadreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'code' => fake()->randomNumber(5, false),
            'titre' => fake()->company(),
            'ponderation' => fake()->numberBetween(0, 100),
            'unite' => fake()->numberBetween(0, 9),
            'attitudes' => fake()->jobTitle(),
            'intentions_educatives'=>fake()->text(250),
            'intentions_pedagogiques'=>fake()->text(500),
            'desc_cours'=>fake()->text(500),
            'changement'=>fake()->date(),
            'approbation_depart'=>fake()->date(),
            'approbation_comite'=>fake()->date(),
            'ponderation_final'=>fake()->numberBetween(50,100),
            'user_id'=>1
        ];
    }
}
