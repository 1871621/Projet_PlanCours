<?php

namespace Tests\Feature;

use App\Models\plancadre;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class plancadreTest extends TestCase
{
    use DatabaseTransactions;
    private plancadre $plancadre;

    public function setUp(): void
    {
        parent::setUp();
        $this->plancadre = plancadre::factory(1)->createOne([
            'titre' => 'Prog 1'
        ]);
    }

    // Test la page index de la classe plancadre
    public function test_index_existe(): void
    {
        $response = $this->get('/plancadre');
        $response->assertStatus(200);
    }

    /**
     * Test qui est possible d'ajouter des donnees
     * @return void
     */
    public function test_ajout_donnes_valides_dans_bd():void
    {
        $this->assertDatabaseMissing("plan_cadres", [
            "code" => '123Test',
            "titre" => 'prog 1 test'
        ]);
        $response = $this->post(route("plancadre.store"), [
            "code" => '123Test',
            "titre" => 'prog 1 test'
        ]);
        $this->assertDatabaseHas("plan_cadres", [
            "code" => '123Test',
            "titre" => 'prog 1 test'
        ]);
    }

    /**
     * test le plancadre.store route
     * @return void
     */
    public function test_ajout_donnes_valides_redirection_liste():void{
        $response = $this->post(route("plancadre.store"), [
            "code" => '123Test',
            "titre" => 'prog 2 test'
        ]);
        $response->assertRedirect(route("plancadre.index"));
    }

    /**
     * Test pour modifier un item de la base de donnee
     * @return void
     */
    public function test_modifier_donnes_dans_bd():void
    {
        $response = $this->post(route("plancadre.update", $this->plancadre->id), [
            "titre" => 'prog 1 test'
        ]);

        $this->assertDatabaseHas("plan_cadres", [
            "titre" => 'prog 1 test'
        ]);
    }


}
