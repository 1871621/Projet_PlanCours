<?php

namespace App\Models;

use Brick\Math\BigInteger;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CritereEvaluation extends Model
{
    use HasFactory;

    public function plancadre(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(PlanCadre::class);
    }

    protected $fillable = [
        'enonce',
        'ponderation'
    ];
}
