<?php

namespace App\Models;

use Brick\Math\BigInteger;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

class PlanCadre extends Model
{
    use HasFactory;

    protected $table = 'plan_cadres';

    public function programme(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Programme::class, 'plancadre_id');
    }

    public function critereevaluation(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CritereEvaluation::class, 'plancadre_id');
    }

    public function competence(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Competence::class, 'plancadres_competences', 'plancadres_id', 'competences_id')->withPivot('complete');
    }

    public function prealable(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(PlanCadre::class,'plancadres_prealable', 'plan_cadre_id', 'prealable_id')->withPivot(['absolu']);
    }

    public function corequi(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(PlanCadre::class,'plancadres_corequi', 'plan_cadre_id', 'corequi_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function joinUser($user_id){
        $this->user()->associate($user_id);
        $this->save();
    }

    protected $fillable = [
        'code',
        'titre',
        'ponderation',
        'unite',
        'attitudes',
        'intentions_educatives',
        'intentions_pedagogiques',
        'desc_cours',
        'changement',
        'approbation_depart',
        'approbation_comite',
        'ponderation_final'
    ];
}
