<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ElementCompetence extends Model
{
    use HasFactory;

    protected $table = 'element_competences';

    public function competence(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Competence::class);
    }

    public function critereperformance(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(CriterePerformance::class, 'element_id');
    }

    protected $fillable = [
        'no',
        'texte',
        'contenu_local'
    ];
}
