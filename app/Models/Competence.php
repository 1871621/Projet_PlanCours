<?php

namespace App\Models;

use Brick\Math\BigInteger;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Competence extends Model
{
    use HasFactory;

    protected $table = 'competences';

    public function plancadres(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(PlanCadre::class);
    }

    public function elementcompetence(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Competence::class, 'competences_id', 'id');
    }

    protected $fillable = [
        'code',
        'titre',
        'enonce',
        'annee_devis',
        'pages_devis',
        'contexte',
        'contexte_local'
    ];
}
