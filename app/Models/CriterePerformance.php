<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CriterePerformance extends Model
{
    use HasFactory;

    public function elementcompetence(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ElementCompetence::class);
    }

    protected $fillable = [
        'no',
        'texte'
    ];
}
