<?php

namespace App\Http\Controllers;

use App\Models\Programme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ProgrammeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('programme.index', ['programme' => Programme::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('programme.nouveau');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validateProgramme($request);
        $programme = Programme::create([
           'code'=>$request->code,
           'nom'=>$request->nom
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Programme $programme)
    {
        return view ('Programme.show', ['Programme' => $programme]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        return view ('programme.modifier', ['programme', Programme::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Programme $programme)
    {
        $this->validateProgramme($request);
        $prog = Programme::where('id', $programme->id)->update([
            'code'=>$request->code,
            'nom'=>$request->nom
        ]);

        $prog->save();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Gate::authorize('isAdmin');
        $programme = Programme::where('id' , $id)->delete();
    }

    public function validateProgramme(Request $request)
    {
        return $request->validate([
            'code'=>'required|string|max:255',
            'nom'=>'requires|string|max:255'
        ]);
    }
}
