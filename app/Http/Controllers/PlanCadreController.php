<?php

namespace App\Http\Controllers;

use App\Models\Competence;
use App\Models\CriterePerformance;
use App\Models\ElementCompetence;
use App\Models\PlanCadre;
use App\Models\Programme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use JavaScript;

class PlanCadreController extends Controller
{
    public function __construct(){
        $this->middleware("auth");
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        // seul les admin peuvent tout voir
        if(Auth::user()->isAdmin == 1){
            $plancadre = PlanCadre::all();
        }
        else{
            $plancadre = PlanCadre::where('user_id', Auth::user()->id)->get();
        }

        return view('plancadre.index', ['plancadres' => $plancadre]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        JavaScript::put([
            'plancadres' => PlanCadre::all(),
            'competences' => Competence::all()
        ]);

        return view('plancadre.nouveau');
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->validatePlanCadre($request);
        $plancadre = PlanCadre::create([
            'code' => $request->code,
            'titre' => $request->titre,
            'ponderation' => $request->ponderation,
            'unite' => $request->unite,
            'intentions_educatives' => $request->intentions_educatives,
            'attitudes' => $request->attitudes,
            'intentions_pedagogiques' => $request->intentions_pedagogiques,
            'desc_cours' => $request->desc_cours,
            'changement' => $request->changement,
            'approbation_depart' => $request->approbation_depart,
            'approbation_comite' => $request->approbation_comite,
            'ponderation_final' => $request->ponderation_final,
        ]);

        $plancadre->user()->associate(Auth::user()->id);

        if(isset($request->prealable)){
            foreach ($request->prealable as $i => $prea){
                $absolu = 0;
                if($request->absolu[$i] == 'on'){
                    $absolu = 1;
                }
                $plancadre->prealable()->attach(PlanCadre::find($prea), ['absolu'=>$absolu]);
            }
        }

        if(isset($request->cours)){
            foreach ($request->cours as $i => $cour){
                $plancadre->corequi()->attach(PlanCadre::find($cour));
            }
        }

        if(isset($request->competence)){
            foreach ($request->competence as $i => $comp){
                $complete = 0;
                if($request->complete[$i] == 'on'){
                    $complete = 1;
                }
                $plancadre->competence()->attach(Competence::find($comp), ['complete'=>$complete]);
            }
        }


        $plancadre->save();
        return redirect()->route('plancadre.index');
    }

    /**
     * Display the specified resource.
     */
    public function show($id): \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        return view ('plancadre.show', ['plancadre' => PlanCadre::find($id), 'elementcompetences' => ElementCompetence::all(), 'critereperformances' => CriterePerformance::all()]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
    {
        JavaScript::put([
            'plancadres' => PlanCadre::all(),
            'competences' => Competence::all()
        ]);

        return view('plancadre.modifier', ['plancadre' => PlanCadre::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PlanCadre $plancadre): \Illuminate\Http\RedirectResponse
    {
        $this->validatePlanCadre($request);

        $plancadre = PlanCadre::where('id', $plancadre->id)->update([
            'code' => $request->code,
            'titre' => $request->titre,
            'ponderation' => $request->ponderation,
            'unite' => $request->unite,
            'intentions_educatives' => $request->intentions_educatives,
            'attitudes' => $request->attitudes,
            'intentions_pedagogiques' => $request->intentions_pedagogiques,
            'desc_cours' => $request->desc_cours,
            'changement' => $request->changement,
            'approbation_depart' => $request->approbation_depart,
            'approbation_comite' => $request->approbation_comite,
            'ponderation_final' => $request->ponderation_final
        ]);

        $plan = PlanCadre::find($plancadre);

        if(isset($request->prealable)){
            foreach ($request->prealable as $i => $prea){
                $absolu = 0;
                if(isset($request->absolu[$i])){
                    if($request->absolu[$i] == 'on'){
                        $absolu = 1;
                    }
                }
                $plan->prealable()->updateExistingPivot($prea, ['absolu' => $absolu]);
            }
        }
/*
        if(isset($request->cours)){
            foreach ($request->cours as $cour){
                $plan->corequi()->updateOrCreate([
                        'plan_cadre_id'=>$cour->plan_cadre_id,
                        'corequi_id'=>$cour->corequi_id
                    ]);
            }
        }*/

        if(isset($request->competence)){
            foreach ($request->competence as $i => $comp){
                $complete = 0;
                if(isset($request->complete[$i])){
                    if($request->complete[$i] == 'on'){
                        $complete = 1;
                    }
                }
                $plan->competence()->updateExistingPivot($comp, ['complete' => $complete]);
            }
        }

        $plan->save();
        return redirect()->route('plancadre.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Gate::authorize('isAdmin');
        $plancadre = PlanCadre::where('id' , $id)->delete();
        return redirect()->route('plancadre.index');
    }

    private function validatePlanCadre(Request $request): array
    {
        return $request->validate([
            'code' => 'required|string|min:6|max:12',
            'titre' => 'required|string|max:255',
            'ponderation' => 'required|string|max:255',
            'unite' => 'required|numeric|between:0.00,99.99',
            'attitudes' =>'nullable|string|max:255',
            'intentions_educatives' =>'nullable|string|max:255',
            'intentions_pedagogiques' =>'nullable|string',
            'desc_cours' =>'nullable|string',
            'changement' =>'nullable|date',
            'approbation_depart' =>'nullable|date',
            'approbation_comite' =>'nullable|date',
            'ponderation_final' =>'nullable|integer'
        ]);
    }
}
