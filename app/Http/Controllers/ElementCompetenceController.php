<?php

namespace App\Http\Controllers;

use App\Models\CriterePerformance;
use App\Models\ElementCompetence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use JavaScript;

class ElementCompetenceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('elementcompetence.index', ['elementcompetence' => ElementCompetence::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        JavaScript::put([
            'critereperformances' => CriterePerformance::all()
        ]);

        return view('elementcompetence.nouveau');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validateElement($request);
        $element = ElementCompetence::create([
            'no' => $request->no,
            'texte' => $request->texte,
            'contenu_local' => $request->contenu_local
        ]);

        if(isset($request->critereperformance)){
            $element->critereperformance()->attach(ElementCompetence::find($request->critereperformance));
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(ElementCompetence $elementCompetence)
    {
        return view ('elementcompetence.show', ['elementcompetence' => $elementCompetence]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        return view('elementcompetence.modifier', ['elementcompetence' => ElementCompetence::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ElementCompetence $elementCompetence)
    {
        $this->validateElement($request);
        $element = ElementCompetence::where('id', $elementCompetence->id)->update([
            'no' => $request->no,
            'texte' => $request->texte,
            'contenu_local' => $request->contenu_local
        ]);

        if(isset($request->critereperformance)){
            $element->critereperformance()->attach(ElementCompetence::find($request->critereperformance));
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Gate::authorize('isAdmin');
        $element = ElementCompetence::where('id' , $id)->delete();
    }

    public function validateElement(Request $request)
    {
        return $request->validate([
            'no' => 'required|string|max:255',
            'texte' => 'required|string|max:255',
            'contenu_local' => 'required|string'
        ]);
    }
}
