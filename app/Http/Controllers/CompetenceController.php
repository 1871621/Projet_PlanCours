<?php

namespace App\Http\Controllers;

use App\Models\Competence;
use App\Models\ElementCompetence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use JavaScript;

class CompetenceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('competence.index', ['competences' => Competence::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        JavaScript::put([
            'elementcompetences' => ElementCompetence::all()
        ]);

        return view ('competence.nouveau');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validateCompetence($request);
        $competence = Competence::create([
            'code' => $request->code,
            'titre' => $request->titre,
            'ennonce' => $request->ennonce,
            'annee_devis' => $request->annee_devis,
            'contexte' => $request->contexte,
            'contexte_local' => $request->contexte_local
        ]);

        // @TODO multiple element competence (parler a la prof)
        if(isset($request->elementcompetence)){
            $competence->elementcompetence()->attach(Competence::find($request->elementcompetence));
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Competence $competence)
    {
        return view ('competence.show', ['Competence' => $competence]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        return view('competence.modifier', ['competence' => Competence::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Competence $competence)
    {
        $this->validateCompetence($request);

        // @TODO Il manque des champs dans le form
        $comp = Competence::where('id', $competence->id)->update([
            'code' => $request->code,
            'titre' => $request->titre,
            'ennonce' => $request->ennonce,
            'annee_devis' => $request->annee_devis,
            'contexte' => $request->contexte,
            'contexte_local' => $request->contexte_local
        ]);

        // @TODO update multiple element competence (parler a la prof)
        if(isset($request->elementcompetence)){
            $comp->elementcompetence()->attach(Competence::find($request->elementcompetence));
        }
        $comp->save();

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Gate::authorize('isAdmin');
        $competence = Competence::where('id' , $id)->delete();
    }

    private function validateCompetence(Request $request): array
    {
        return $request->validate([
            'code' => 'required|string|min:6|max:12',
            'titre' => 'required|string|max:255',
            'annee_devis' => 'integer',
            'ennonce' => 'required|string|max:255',
            'page_devis' =>'nullable|string|max:255',
            'contexte' =>'string|nullable',
            'contexte_local' =>'nullable|string'
        ]);
    }
}
