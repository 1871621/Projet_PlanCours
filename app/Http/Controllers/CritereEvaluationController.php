<?php

namespace App\Http\Controllers;

use App\Models\CritereEvaluation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CritereEvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('CritereEvaluation.index', ['critereEvals' => CritereEvaluation::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('critereevaluation.nouveau');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(CritereEvaluation $critereEvaluation)
    {
        return view ('CritereEvaluation.show', ['CritereEvaluation' => $critereEvaluation]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(CritereEvaluation $critereEvaluation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, CritereEvaluation $critereEvaluation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(CritereEvaluation $critereEvaluation)
    {
        Gate::authorize('isAdmin');
    }
}
