<?php

namespace App\Http\Controllers;

use App\Models\CriterePerformance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CriterePerformanceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('critereperformance.index', ['critereperformance' => CriterePerformance::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('critereperformance.nouveau');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validateCritere($request);
        $critere = CriterePerformance::create([
            'no' => $request->no,
            'texte' => $request->texte
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(CriterePerformance $criterePerformance)
    {
        return view ('critereperformance.show', ['critereperformance' => $criterePerformance]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        return view('critereperformance.modifier', ['critereperformance' => CriterePerformance::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, CriterePerformance $criterePerformance)
    {
        $this->validateCritere($request);
        $critere = CriterePerformance::where('id', $criterePerformance->id)->update([
            'no' => $request->no,
            'texte' => $request->texte
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        Gate::authorize('isAdmin');
        $criterePerformance = CriterePerformance::where('id' , $id)->delete();
    }

    public function validateCritere(Request $request)
    {
        return $request->validate([
            'no' => 'required|string|max:255',
            'texte' => 'required|string|max:255'
        ]);
    }
}
