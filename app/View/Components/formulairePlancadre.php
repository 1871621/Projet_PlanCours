<?php

namespace App\View\Components;

use App\Models\Competence;
use App\Models\PlanCadre;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;

class formulairePlancadre extends Component
{
    public Collection $competence;
    public Collection $plancadres;
    public string $textBoutton;

    /**
     * Create a new component instance.
     */
    public function __construct(public string $texteBoutton = "Enregistrer", public ?PlanCadre $plancadre = null)
    {
        $this->textBoutton = $texteBoutton;
        $this->competence = Competence::all();
        $this->plancadres = PlanCadre::all();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('plancadre.formulaire');
    }
}
