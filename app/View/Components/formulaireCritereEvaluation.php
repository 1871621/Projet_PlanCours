<?php

namespace App\View\Components;

use App\Models\CritereEvaluation;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class formulaireCritereEvaluation extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(public string $texteBoutton = "Enregistrer", public ?CritereEvaluation $critereEvaluation = null)
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('critereevaluation.formulaire');
    }
}
