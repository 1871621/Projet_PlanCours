<?php

namespace App\View\Components;

use Illuminate\Database\Eloquent\Collection;
use App\Models\CriterePerformance;
use App\Models\ElementCompetence;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class formulaireElementCompetence extends Component
{
    public Collection $criterePerformance;
    public string $textBoutton;

    /**
     * Create a new component instance.
     */
    public function __construct(public string $texteBoutton = "Enregistrer", public ?ElementCompetence $elementcompetence = null)
    {
        $this->textBoutton = $this->texteBoutton;
        $this->criterePerformance = CriterePerformance::all();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('elementcompetence.formulaire');
    }
}
