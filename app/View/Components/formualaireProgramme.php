<?php

namespace App\View\Components;

use App\Models\Programme;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class formualaireProgramme extends Component
{
    public string $textBoutton;

    /**
     * Create a new component instance.
     */
    public function __construct(public string $texteBoutton = "Enregistrer", public ?Programme $programme = null)
    {
        $this->textBoutton = $texteBoutton;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('programme.formulaire');
    }
}
