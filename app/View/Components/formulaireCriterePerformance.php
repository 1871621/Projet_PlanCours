<?php

namespace App\View\Components;

use App\Models\CriterePerformance;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class formulaireCriterePerformance extends Component
{
    public string $textBoutton;
    /**
     * Create a new component instance.
     */
    public function __construct(public string $texteBoutton = "Enregistrer", public ?CriterePerformance $critereperformance = null)
    {
        $this->textBoutton = $this->texteBoutton;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('critereperformance.formulaire');
    }
}
