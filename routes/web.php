<?php

use App\Http\Controllers\CompetenceController;
use App\Http\Controllers\CritereEvaluationController;
use App\Http\Controllers\CriterePerformanceController;
use App\Http\Controllers\ElementCompetenceController;
use App\Http\Controllers\PlanCadreController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProgrammeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::resource('competence', CompetenceController::class, [
    'parameters' => ['Competence' => 'competence']
])->only(['index', 'create', 'store', 'edit', 'update', 'destroy', 'show']);

Route::resource('critereevaluation', CritereEvaluationController::class, [
    'parameters' => ['CritereEvaluation' => 'critereevaluation']
])->only(['index', 'create', 'store', 'edit', 'update', 'destroy', 'show']);

Route::resource('critereperformance', CriterePerformanceController::class, [
    'parameters' => ['CriterePerformance' => 'critereperformance']
])->only(['index', 'create', 'store', 'edit', 'update', 'destroy', 'show']);

Route::resource('elementcompetence', ElementCompetenceController::class, [
    'parameters' => ['ElementCompetence' => 'elementcompetence']
])->only(['index', 'create', 'store', 'edit', 'update', 'destroy', 'show']);

Route::resource('plancadre', PlanCadreController::class, [
    'parameters' => ['PlanCadre' => 'plancadre']
])->only(['index', 'create', 'store', 'edit', 'update', 'destroy', 'show']);

Route::resource('profil', ProfileController::class);

Route::resource('programme', ProgrammeController::class, [
    'parameters' => ['Programme' => 'programme']
])->only(['index', 'create', 'store', 'edit', 'update', 'destroy', 'show']);


Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
