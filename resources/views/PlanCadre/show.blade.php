<x-app-layout>
    <div class="container">
        <div content="row">
            <div class="col">
                <h1>{{$plancadre->code}} {{$plancadre->titre}}</h1>
                <hr>
                {{-- @TODO Integrer l'image du cegep --}}
                <img src="">
                <h2>Plan-cadre</h2>
                <p>Titre du cours : {{$plancadre->titre}}</p>
                <p>Numero : {{$plancadre->code}}</p>
                <hr>
                {{-- Page principale --}}
                <p><b>ponderation : </b> {{$plancadre->ponderation}}</p>
                <p><b>Unité : </b>{{$plancadre->unite}}</p>
                {{-- @TODO Prealable --}}
                @foreach($plancadre->prealable as $pc)
                    @if($plancadre->absolu == 1)
                        <p><b>Prealable absolu : </b>{{$pc->code}} {{$pc->titre}} {{$pc->ponderation}}</p>
                    @elseif($plancadre->absolu == 0)
                        <p><b>Prealable : </b>{{$pc->code}} {{$pc->titre}} {{$pc->ponderation}}</p>
                    @endif
                @endforeach
                @foreach($plancadre->corequi as $pc)
                    <p><b>Cours co-requis : </b>{{$pc->code}} {{$pc->titre}} {{$pc->ponderation}}</p>
                @endforeach
                {{-- @TODO Competence (atteinte a voir?) --}}
                <table class="table">
                    <tr>
                        <th class="col">Compétence</th>
                        <th class="col">No</th>
                        <th class="col">Atteinte</th>
                    </tr>
                    @foreach($plancadre->competence as $competence)
                        <tr>
                            <td class="row">{{$competence->titre}}</td>
                            <td>{{$competence->code}}</td>
                            <td>{{$competence->ennonce}}</td>
                        </tr>
                    @endforeach
                </table>
                <p><b>Intentions éducatives, </b>Pages 7 et 8 du devis 2017</p>
                <p>Les intentions éducatives en formation spécifique s'appuient sur des valeurs
                    et préoccupations importantes et qui servent de guide aux interventions auprès de
                    l'étudiant ou de l'étudiante. Elles touchent généralement des dimensions significatives
                    du développement professionnel et personnel qui n'ont pas fait l'objet de formulations
                    explicites au niveau des buts de la formation ou des objectifs et standards. Elles
                    peuvent porter sur des attitudes importantes, des habitudes de travail, des habiletés
                    intellectuelles, etc.</p>
                <p>Pour le programme Technique de l'informatique, les intentions éducatives en formation
                    spécifique sont les suivantes :</p>
                <p>{{$plancadre->intentions_educatives}}</p>

                <p><b>Intentions pédagogiques</b></p>
                <p>{{$plancadre->intentions_pedagogiques}}</p>

                <p><b>Brève description du cours</b></p>
                <p>{{$plancadre->desc_cours}}</p>

                <table class="table table-bordered">
                    <tr>
                        <th class="col">OBJECTIF</th>
                        <th class="col">STANDARD</th>
                        <th class="col">PRÉCISIONS SUR LES CONTENUS</th>
                    </tr>
                    <tr>
                        <td><b>Énnoncé de la competence</b></td>
                        <td><b>Context de réalisation</b>, devis 2017, pp 67-68</td>
                        <td><b>Contexte de réalisation local</b></td>
                    </tr>
                    @foreach($plancadre->competence as $competence)
                        <tr>
                            <td>{{$competence->enonce}}</td>
                            <td>{{$competence->contexte}}</td>
                            <td>{{$competence->contexte_local}}</td>
                        </tr>
                    @endforeach
                    @foreach($plancadre->competence as $competence)
                    <tr>
                        <td colspan="3">
                                <p class="d-flex justify-content-center">Attitudes à enseigner et à évaluer pour la compétence {{$competence->code}}</p>
                                <p class="d-flex justify-content-center">autonomie, rigueur, collaboration et efficacite</p>
                                {{-- @TODO Mettre les element de competence --}}
                        </td>
                    </tr>
                    @endforeach
                </table>

                <table class="table table-bordered">

                    <tr>
                        <th class="col">Éléments de compétence</th>
                        <th class="col">Critère de performance</th>
                        <th class="col">Contenus locaux</th>
                    </tr>
                    <?php $compte1 = 1 ?>
                    @foreach($plancadre->competence as $competence)
                        @foreach($elementcompetences as $element)
                            @if($element->competence_id == $competence->id)
                                <tr>
                                    {{-- element de la competence --}}
                                    <td>{{$compte1}}. {{$element->texte}}</td>

                                    {{-- Critere performance --}}
                                    <?php $compte2 = 1 ?>
                                    <td>
                                        @foreach($critereperformances as $critere)
                                            @if($critere->elementcompetence_id == $element->id)
                                                {{$compte1}}.{{$compte2}} {{$critere->texte}}<br>
                                                <?php $compte2 += 1 ?>
                                            @endif
                                        @endforeach
                                    </td>

                                    {{-- contenu local --}}
                                    <td>{{$element->contenu_local}}</td>
                                </tr>
                                <?php $compte1 += 1 ?>
                            @endif
                        @endforeach
                    @endforeach

                </table>

                <div>
                    <p>Date d'adoption par le département 420 : {{$plancadre->approbation_depart}}</p>
                    <p>Date d'adoption par le Comité de programme : {{$plancadre->approbation_comite}}</p>
                    <p>Date du dépôt à la Direction des études : Techniques de l'informatique (420.B0.PS)</p>
                </div>

            </div>

            <div class="col d-flex align-items-center flex-column">
                <a class="btn btn-primary p-2" href="">Imprimer</a>
                <a class="btn btn-primary p-2" href="{{route('plancadre.edit', $plancadre->id)}}">Modifier</a>
                <button class="btn btn-primary p-2" href="" disabled>Plans de cours</button>
                <a class="btn btn-primary p-2" href="">Archiver</a>
                <a class="btn btn-primary p-2" href="{{route('plancadre.destroy', $plancadre->id)}}">Supprimer</a>
            </div>
        </div>
    </div>
</x-app-layout>
