@include ('footer')
<x-app-layout>
    <h1>Nouveau Plan Cadre</h1>
    <form method="POST" action="{{route('plancadre.store')}}">
        @csrf
        <x-formulaire-plancadre texte-bouton="Nouveau"></x-formulaire-plancadre>
    </form>
</x-app-layout>
