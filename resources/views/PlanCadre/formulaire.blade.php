{{-- Code inspirer de la prof Pour le formulaire --}}

{{-- Code --}}
<div class="my-3">
    <label for="code" class="form-label">No</label>
    <input required value="{{$plancadre?->code}}" name="code" id="code" type="text" placeholder="420-4P6-HU" class="form-control">
    @error('code')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Ponderation --}}
<div class="my-3">
    <label for="ponderation" class="form-label">Ponderation</label>
    <input required value="{{$plancadre?->ponderation}}" name="ponderation" id="ponderation" type="text" class="form-control">
    @error('ponderation')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Ponderation_final --}}
<div class="my-3">
    <label for="ponderation_final" class="form-label">Ponderation final</label>
    <input required value="{{$plancadre?->ponderation}}" name="ponderation_final" id="ponderation_final" type="text" class="form-control">
    @error('ponderation_final')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- unite --}}
<div class="my-3">
    <label for="unite" class="form-label">Unites</label>
    <input required value="{{$plancadre?->unite}}" type="number" step="0.001" name="unite" id="unite" class="form-control">
    @error('unite')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Titre --}}
<div class="my-3">
    <label for="titre" class="form-label">Titre</label>
    <input required value="{{$plancadre?->titre}}" name="titre" id="titre" class="form-control">
    @error('titre')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Attitudes --}}
<div class="my-3">
    <label for="attitudes" class="form-label">Attitudes</label>
    <input name="attitudes" id="attitudes" class="form-control">
    @error('attitudes')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- desc_cours --}}
<div class="my-3">
    <div class="pt-4">
        <label for="desc_cours" class="form-label">Description du cours</label>
        <textarea id="desc_cours" name="desc_cours" rows="4" class="form-control"></textarea >
    </div>
    @error('desc_cours')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Prealable --}}
<div class="my-3">
    <div class="flex">
        <label for="prealable_plus" class="form-label">Prealable(s)</label>
        <button id="prealable_plus" class="btn btn-secondary rounded-circle btn-sm" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
            </svg>
        </button>
    </div>
    <?php  // donner uve id unique a toutes les composantes pour pas creer de trouble au submit
    $compte = 1; ?>
    <div id="prealable_select">
        @foreach($plancadre?->prealable as $prealable)
            <div id="prealables">
                <select name="prealable[]" id="prealable{{$compte}}" class="form-select">
                    <option value="">---</option>
                    @foreach($plancadres as $plan)
                        @if($prealable->id == $plan->id)
                            <option value="{{$plan->id}}" selected>{{$plan->titre}} - {{$plan->code}}</option>
                        @else
                            <option value="{{$plan->id}}">{{$plan->titre}} - {{$plan->code}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <!-- Rounded switch  -->
            <div class="form-check form-switch">
                @if($plancadre->prealable()->where('plan_cadre_id', $plancadre->id)->firstOrFail()->pivot->absolu == 1)
                    <input class="form-check-input" type="checkbox" role="switch" id="absolu{{$compte}}" name="absolu[]" checked>
                    <label class="form-check-label" for="absolu{{$compte}}">Absolu</label>
                @else
                    <input class="form-check-input" type="checkbox" role="switch" id="absolu{{$compte}}" name="absolu[]">
                    <label class="form-check-label" for="absolu{{$compte}}">Absolu</label>
                @endif
            </div>
            <?php $compte += 1 ?>
        @endforeach
        @error('prealable')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>


</div>

{{-- cours --}}
<div class="my-3">
    <div class="flex">
        <label for="cours_plus" class="form-label" >Cours co-requis</label>
        <button id="cours_plus" class="btn btn-secondary rounded-circle btn-sm" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
            </svg>
        </button>
    </div>
    <?php  // donner une id unique a toutes les composantes pour pas creer de trouble au submit
    $compte = 1; ?>
    <div id="cours_select">
        @foreach($plancadre?->corequi as $corequi)
            <select id="cours{{$compte}}" name="cours[]" class="form-select">
                <option value="">---</option>
                @foreach($plancadres as $plan)
                    @if($corequi->id == $plan->id)
                        <option value="{{$plan->id}}" selected>{{$plan->titre}} - {{$plan->code}}</option>
                    @else
                        <option value="{{$plan->id}}" >{{$plan->titre}} - {{$plan->code}}</option>
                    @endif
                @endforeach
            </select>
            <?php $compte += 1 ?>
        @endforeach
    </div>

    <span class="flex-1"></span>
    @error('cours')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Competence --}}
<div class="my-3">
    <div class="flex">
        <label for="competence_plus" class="form-label">Competence(s)</label>
        <button id="competence_plus" class="btn btn-secondary rounded-circle btn-sm" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
            </svg>
        </button>
        <a href="{{route('competence.create')}}" type="button" class="btn btn-secondary btn-sm">Nouvelle</a>
    </div>
    <?php  // donner une id unique a toutes les composantes pour pas creer de trouble au submit
    $compte = 1; ?>
    <div id="competence_select">
        @foreach($plancadre?->competence as $comp)
            <select id="competence{{$compte}}" name="competence[]" class="form-select">
                <option value="">---</option>
                @foreach($competence as $competences)
                    @if($competences->id == $comp->id)
                        <option value="{{$competences->id}}" selected>{{$competences->titre}} - {{$competences->code}}</option>
                    @else
                        <option value="{{$competences->id}}">{{$competences->titre}} - {{$competences->code}}</option>
                    @endif
                @endforeach
            </select>
            <div class="form-check form-switch">
                @if($plancadre->competence()->where('plancadres_id', $plancadre->id)->firstOrFail()->pivot->complete == 0)
                    <input class="form-check-input" type="checkbox" role="switch" id="complete{{$compte}}" name="complete[]">
                    <label class="form-check-label" for="complete{{$compte}}">Complete</label>
                @else
                    <input class="form-check-input" type="checkbox" role="switch" id="complete{{$compte}}" name="complete[]" checked>
                    <label class="form-check-label" for="complete{{$compte}}">Complete</label>
                @endif
            </div>
            <?php $compte += 1 ?>
        @endforeach
    </div>

    @error('competence')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Intentions_educatives --}}
<div class="my-3">
    <div class="pt-4">
        <label for="intentions_educatives" class="form-label">Intentions Educatives</label>
        <textarea id="intentions_educatives" name="intentions_educatives" rows="4" class="form-control"></textarea >
    </div>
    @error('intentions_educatives')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- intentions_pedagogiques --}}
<div class="my-3">
    <div class="pt-4">
        <label for="intentions_pedagogiques" class="form-label">intentions_Pedagogiques</label>
        <textarea id="intentions_pedagogiques" name="intentions_pedagogiques" rows="4" class="form-control"></textarea >
    </div>
    @error('intentions_pedagogiques')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Changement --}}
<div class="my-3">
    <label for="changement" class="form-label">Changement</label>
    <input name="changement" id="changement" class="form-control" type="date">
    @error('changement')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- approbation_depart --}}
<div class="my-3">
    <label for="approbation_depart" class="form-label">Approbation de depart</label>
    <input name="approbation_depart" id="approbation_depart" class="form-control" type="date">
    @error('approbation_depart')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- approbation_comite --}}
<div class="my-3">
    <label for="approbation_comite" class="form-label">Approbation de comite</label>
    <input name="approbation_comite" id="approbation_comite" class="form-control" type="date">
    @error('approbation_comite')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>


{{-- Boutton, c juste un boutton, voila --}}
<div class="my-3">
    <button type="submit" class="btn btn-primary">{{$textBoutton}}</button>
</div>
