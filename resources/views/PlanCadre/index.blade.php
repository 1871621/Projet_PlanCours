<x-app-layout>
    <div class="flex">
        <div class="">
            <div class="">
                <div class="">
                    <h1 class="">Ceci est la liste de plan cadre</h1>
                </div>
                @auth
                <div class="">
                    <a href="{{route('plancadre.create')}}" class="btn-primary">Ajouter</a>
                </div>
                @endauth
            </div>
            <br>
            <table class="table table-striped">
                <tbody>
                    @foreach($plancadres as $plancadre)
                        <tr>
                            <th scope="row" >
                                <p>{{$plancadre->code}} {{$plancadre->titre}} ({{$plancadre->ponderation}})</p>
                            </th>
                            <td>
                                <a href="{{route('plancadre.show', $plancadre->id)}}">Voir</a>
                            </td>
                            <td>
                                <a href="{{route('plancadre.edit', $plancadre->id)}}">Modifier</a>
                            </td>
                            @can('isAdmin')
                                <td>
                                    <a href="{{route('plancadre.destroy', $plancadre->id)}}">Effacer</a>
                                </td>
                            @endcan
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</x-app-layout>

