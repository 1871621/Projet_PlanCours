@include ('footer')
<x-app-layout>
    <h1>Modifier {{$plancadre->titre}}</h1>
    <form method="post" action="{{route('plancadre.update', ['plancadre' => $plancadre->id])}}">
        @csrf
        @method('PUT')
        <x-formulaire-plancadre texte-bouton="Modifier" :plancadre="$plancadre"></x-formulaire-plancadre>
    </form>
</x-app-layout>
