@include ('footer')
<x-app-layout>
    <h1>Nouveau critere de performance</h1>
    <form method="POST" action="{{route('critereperformance.store')}}">
        @csrf
        <x-formulaire-critere-performance> texte-bouton="Nouveau"></x-formulaire-critere-performance>
    </form>
</x-app-layout>
