{{-- No --}}
<div class="my-3">
    <label for="no" class="form-label">Titre</label>
    <input required name="no" id="no" class="form-control">
    @error('no')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Texte --}}
<div class="my-3">
    <label for="texte" class="form-label">Titre</label>
    <input required name="texte" id="texte" class="form-control">
    @error('texte')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<button type="submit" class="btn btn-primary">{{$textBoutton}}</button>
