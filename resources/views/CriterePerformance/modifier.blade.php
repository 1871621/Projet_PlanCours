@include ('footer')
<x-app-layout>
    <h1>Modifier {{$critereperformance->no}}</h1>
    <form method="post" action="{{route('critereperformance.update', ['critereperformance' => $critereperformance->id])}}">
        @csrf
        @method('PUT')
        <x-formulaire-critere-performance texte-bouton="Modifier" :critereperformance="$critereperformance"></x-formulaire-critere-performance>
    </form>
</x-app-layout>
