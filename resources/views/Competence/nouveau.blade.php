<x-app-layout>
    <h1>Nouvelle competence</h1>
    <form method="post" action="{{route('competence.store')}}">
        @csrf
        <x-formulaire-competence texte-bouton="Nouveau"></x-formulaire-competence>
    </form>
</x-app-layout>
