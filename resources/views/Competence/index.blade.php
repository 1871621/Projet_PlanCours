<x-app-layout>
    <div class="flex">
        <div class="">
            <div class="">
                <div class="">
                    <h1 class="">Ceci est la liste de competence</h1>
                </div>
                @auth
                    <div class="">
                        <a href="{{route('competence.create')}}" class="btn-primary">Ajouter</a>
                    </div>
                @endauth
            </div>
            <br>
            <table class="table table-striped">
                <tbody>
                @foreach($competences as $competence)
                    <tr>
                        <th scope="row" >
                            <p>{{$competence->code}} {{$competence->titre}}</p>
                        </th>
                        <td>
                            <a href="{{route('competence.show', $competence->id)}}">Voir</a>
                        </td>
                        <td>
                            <a href="{{route('competence.edit', $competence->id)}}">Modifier</a>
                        </td>
                        <td>
                            <a href="{{route('competence.destroy', $competence->id)}}">Effacer</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>
