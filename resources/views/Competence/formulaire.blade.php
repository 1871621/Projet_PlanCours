{{-- @TODO Formulaire pour ajouter et modifier
Code inspirer de la prof Pour le formulaire --}}

{{-- Code --}}
<div class="my-3">
    <label for="code" class="form-label">No</label>
    <input required name="code" id="code" type="text" placeholder="420-4P6-HU" class="form-control">
    @error('code')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Titre --}}
<div class="my-3">
    <label for="titre" class="form-label">Titre</label>
    <input required name="titre" id="titre" class="form-control">
    @error('titre')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Ennonce --}}
<div class="my-3">
    <label for="ennonce" class="form-label">Ennonce</label>
    <input required name="ennonce" id="ennonce" class="form-control">
    @error('ennonce')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Context --}}
<div class="my-3">
    <label for="context" class="form-label">Context</label>
    <input required name="context" id="context" class="form-control">
    @error('context')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Context_local --}}
<div class="my-3">
    <label for="context_local" class="form-label">Context local</label>
    <input required name="context_local" id="context_local" class="form-control">
    @error('context_local')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Annee_devis --}}
<div class="my-3">
    <label for="annee_devis" class="form-label">Annee de devis?</label>
    <input required name="annee_devis" id="context" class="form-control" type="number">
    @error('annee_devis')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Page_devis --}}
<div class="my-3">
    <label for="pages_deivs" class="form-label">Page de devis?</label>
    <input required name="pages_devis" id="pages_devis" class="form-control">
    @error('pages_devis')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Element Competence --}}
<div class="my-3">
    <div class="flex">
        <label for="elementcompetence_plus" class="form-label">Element de Competence(s)</label>
        <button id="elementcompetence_plus" class="btn btn-secondary rounded-circle btn-sm" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
            </svg>
        </button>
        <a href="{{route('elementcompetence.create')}}" type="button" class="btn btn-secondary btn-sm">Nouvelle</a>
    </div>
    <?php  // donner une id unique a toutes les composantes pour pas creer de trouble au submit
    $compte = 1; ?>
    <div id="elementcompetence_select">
        @foreach($competence?->elementcompetence as $comp)
            <select id="elementcompetence{{$compte}}" name="elementcompetence" class="form-select">
                <option value="">---</option>
                @foreach($elementcompetences as $element)
                    @if($element->id == $comp->id)
                        <option value="{{$element->id}}" selected>{{$element->titre}} - {{$element->code}}</option>
                    @else
                        <option value="{{$element->id}}">{{$element->titre}} - {{$element->code}}</option>
                    @endif
                @endforeach
            </select>
            <?php $compte += 1 ?>
        @endforeach
    </div>

    @error('competence')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>


<button type="submit" class="btn btn-primary">{{$textBoutton}}</button>
