<x-app-layout>
    <h1>Modifier {{$competence->titre}}</h1>
    <form method="post" action="{{route('competence.update', ['competence'=>$competence->id])}}">
        @csrf
        @method('PUT')
        <x-formulaire-competence texte-bouton="Modifier" :competence="$competence"></x-formulaire-competence>
    </form>
</x-app-layout>
