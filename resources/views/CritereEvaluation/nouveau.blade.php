<x-app-layout>
    <h1>Nouvelle competence</h1>
    <form method="post" action="{{route('critereevaluation.create')}}">
        @csrf
        <x-formulaire-critere-evaluation texte-bouton="Nouveau"></x-formulaire-critere-evaluation>
    </form>
</x-app-layout>
