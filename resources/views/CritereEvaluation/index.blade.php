<x-app-layout>
    <div class="flex">
        <div class="">
            <div class="">
                <div class="">
                    <h1 class="">Ceci est la liste de critere d'evaluation</h1>
                </div>
                @auth
                    <div class="">
                        <a href="{{route('CritereEvaluation.create')}}" class="btn-primary">Ajouter</a>
                    </div>
                @endauth
            </div>
            <br>
            <table class="table table-striped">
                <tbody>
                @foreach($critereEvals as $critereEval)
                    <tr>
                        <th scope="row" >
                            <p>{{$critereEval->ennonce}} {{$critereEval->ponderation}}</p>
                        </th>
                        <td>
                            <a href="{{route('CritereEvaluation.show', $critereEval->id)}}">Voir</a>
                        </td>
                        <td>
                            <a href="{{route('CritereEvaluation.edit', $critereEval->id)}}">Modifier</a>
                        </td>
                        <td>
                            <a href="{{route('CritereEvaluation.destroy', $critereEval->id)}}">Effacer</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>
