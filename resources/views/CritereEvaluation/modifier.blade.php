<x-app-layout>
    <h1>Modifier {{$critereevaluation->ennonce}}</h1>
    <form method="post" action="{{route('critereevaluation.update', ['critereevaluation'=>$critereevaluation->id])}}">
        @csrf
        @method('PUT')
        <x-formulaire-critere-evaluation texte-bouton="Modifier" :critereevaluation="$critereevaluation"></x-formulaire-critere-evaluation>
    </form>
</x-app-layout>
