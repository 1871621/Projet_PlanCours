{{-- ennonce --}}
<div class="my-3">
    <label for="ennonce" class="form-label">No</label>
    <input required name="ennonce" id="ennonce" type="text" placeholder="420-4P6-HU" class="form-control">
    @error('ennonce')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Code --}}
<div class="my-3">
    <label for="ponderation" class="form-label">No</label>
    <input required name="ponderation" id="ponderation" type="text" placeholder="420-4P6-HU" class="form-control">
    @error('ponderation')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

<button type="submit" class="btn btn-primary">{{$texteBoutton}}</button>
