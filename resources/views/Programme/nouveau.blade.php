@include ('footer')
<x-app-layout>
    <h1>Nouveau programme</h1>
    <form method="POST" action="{{route('programme.store')}}">
        @csrf
        <x-formulaire-programme texte-bouton="Nouveau"></x-formulaire-programme>
    </form>
</x-app-layout>
