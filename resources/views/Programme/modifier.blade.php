@include ('footer')
<x-app-layout>
    <h1>Modifier {{$programme->titre}}</h1>
    <form method="post" action="{{route('programme.update', ['programme' => $programme->id])}}">
        @csrf
        @method('PUT')
        <x-formulaire-programme texte-bouton="Modifier" :programme="$programme"></x-formulaire-programme>
    </form>
</x-app-layout>
