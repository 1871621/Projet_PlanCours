@props(['status'])

@if ($status)
    <div {role="alert" {{ $attributes->merge(['class' => 'alert alert-success']) }} }>
        {{ $status }}
    </div>
@endif
