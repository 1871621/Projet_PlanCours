<button type="button" class="btn btn-primary" id="liveToastBtn">{{$texteBouton}}</button>

<div class="toast-container position-fixed bottom-0 end-0 p-3">
    <div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            {{$entete}}
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            <h2>{{$chat->nom}}</h2>
            {{$slot}}
        </div>
    </div>
</div>
