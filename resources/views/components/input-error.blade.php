@props(['messages'])

@if ($messages)
    <ul {{ $attributes->merge(['class' => 'alert alert-danger ps-2 list-group list-unstyled']) }}>
        @foreach ((array) $messages as $message)
            <li class="list-item">{{ $message }}</li>
        @endforeach
    </ul>
@endif
