<nav class="navbar navbar-expand-sm navbar-dark bg-dark" data-bs-theme="blue">
    <div class="container-fluid">
        <!-- logo -->
        <div class="shrink-0 flex items-center">
            <a class="nav-link"
               @class(['active' => request()->routeIs('dashboard')])
               href="{{ route('dashboard') }}">
                <img src="public/img/logo.png" alt="Logo du cegep" height="50" width="150">
            </a>
        </div>


        <div class="collapse navbar-collapse" id="mynavbar">
            <!-- Les items a gauche de la navbar -->
            <ul class="navbar-nav me-auto">
                @auth
                <li class="nav-item" style="margin-top:-12px;">
                    <a class="nav-link"
                       @class(['active' => request()->routeIs('plancadre.index')])
                       href="{{ route('plancadre.index') }}"> Liste de Plan Cadre
                    </a>
                </li>
                <li class="nav-item" style="margin-top:-12px;">
                    <a class="nav-link"
                       @class(['active' => request()->routeIs('Competence.index')])
                       href="{{ route('competence.index') }}"> Liste de Competence
                    </a>
                </li>
                @endauth
            </ul>
            <!-- les items a droite de la navbar -->
            <ul class="navbar-nav d-flex" style="margin-top: -12px">
                @auth
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            {{ Auth::user()->name }}
                        </a>
                        <!-- dropdown des items de profils -->
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{route('profile.edit')}}">{{ __('Profile') }}</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li>
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <button type="submit" class="dropdown-item">{{ __('Log Out') }}</button>
                                </form>
                            </li>
                        </ul>
                    </li>
                @endauth
                @guest
                    <a class="nav-link"
                       @class(['active' => request()->routeIs('login')])
                       href="{{ route('login') }}">{{ __('Login') }}
                    </a>

                    <a class="nav-link"
                       @class(['active' => request()->routeIs('register')])
                       href="{{ route('register') }}">{{ __('Register') }}
                    </a>
                @endguest
            </ul>
        </div>
    </div>
</nav>
