@include ('footer')
<x-app-layout>
    <h1>Nouveau element de competence</h1>
    <form method="POST" action="{{route('elementcompetence.store')}}">
        @csrf
        <x-formulaire-element-competence texte-bouton="Nouveau"></x-formulaire-element-competence>
    </form>
</x-app-layout>
