{{-- No --}}
<div class="my-3">
    <label for="no" class="form-label">Titre</label>
    <input required name="no" id="no" class="form-control">
    @error('no')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Texte --}}
<div class="my-3">
    <label for="texte" class="form-label">Titre</label>
    <input required name="texte" id="texte" class="form-control">
    @error('texte')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
</div>

{{-- Critere Performance --}}
<div class="my-3">
    <div class="flex">
        <label for="critereperformance_plus" class="form-label">Critere de performance</label>
        <button id="critereperformance_plus" class="btn btn-secondary rounded-circle btn-sm" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
            </svg>
        </button>
        <a href="{{route('critereperformance.create')}}" type="button" class="btn btn-secondary btn-sm">Nouvelle</a>
    </div>
    <?php  // donner une id unique a toutes les composantes pour pas creer de trouble au submit
    $compte = 1; ?>
    <div id="critereperformance_select">{{--}}
        @if(isset($elementcompetence))
        @foreach($elementcompetence?->critereperformance as $critere)
            <select id="critereperformance{{$compte}}" name="critereperformance" class="form-select">
                <option value="">---</option>
                @foreach($competence as $competences)
                    @if($competences->id == $comp->id)
                        <option value="{{$competences->id}}" selected>{{$competences->titre}} - {{$competences->code}}</option>
                    @else
                        <option value="{{$competences->id}}">{{$competences->titre}} - {{$competences->code}}</option>
                    @endif
                @endforeach
            </select>
            <?php $compte += 1 ?>
        @endforeach
        @endif{{--}}
    </div>

    @error('competence')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

</div>

<button type="submit" class="btn btn-primary">{{$textBoutton}}</button>
