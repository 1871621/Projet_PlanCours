@include ('footer')
<x-app-layout>
    <h1>Modifier {{$elementcompetence->no}}</h1>
    <form method="post" action="{{route('elementcompetence.update', ['elementcompetence' => $elementcompetence->id])}}">
        @csrf
        @method('PUT')
        <x-formulaire-element-competence texte-bouton="Modifier" :elementcompetence="$elementcompetence"></x-formulaire-element-competence>
    </form>
</x-app-layout>
