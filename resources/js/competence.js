const elementcompetence = document.getElementById('elementcompetence_plus');


/**
 * La function quand on click sur le petit plus sur la section prealable
 */
elementcompetence.onclick = function () {
    let div = document.getElementById('elementcompetence_select')
    let select = document.createElement('SELECT');
    select.classList.add("form-select")
    // aller trouver quelle va etre le id du select et de la switch
    let nombreEnfant = div.childElementCount;
    let nombre = nombreEnfant + 1;
    select.id = 'elementcompetence' + nombre;

    //option null
    let option;
    option = document.createElement('OPTION');
    option.setAttribute('value', null);
    option.appendChild(document.createTextNode('---'));
    select.appendChild(option);

    //mettre toutes les options
    for(const comp of elementcompetences){
        option = document.createElement('OPTION');
        option.setAttribute('value', comp.id);
        option.appendChild(document.createTextNode(comp.no + ' ' + comp.texte));
        select.appendChild(option);
    }

    if(nombre<6){
        div.appendChild(select);
    }
}
