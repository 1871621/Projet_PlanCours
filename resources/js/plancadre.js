const prealable = document.getElementById('prealable_plus');
const cours = document.getElementById('cours_plus');
const competence = document.getElementById('competence_plus');

/**
 * La function quand on click sur le petit plus sur la section prealable
 */
prealable.onclick = function () {
    let div = document.getElementById('prealable_select')
    let select = document.createElement('SELECT');
    select.classList.add("form-select")
    // aller trouver quelle va etre le id du select et de la switch
    let nombreEnfant = div.childElementCount;
    let nombre = nombreEnfant/2 + 1;
    select.id = 'prealable' + nombre;
    select.setAttribute('name', 'prealable[]');

    // un autre div
    let prealables = document.createElement('DIV');
    prealables.id = "prealables";

    //option null
    let option;
    option = document.createElement('OPTION');
    option.setAttribute('value', null);
    option.appendChild(document.createTextNode('---'));
    select.appendChild(option);

    //mettre tout les options
    for(const plan of plancadres){
        option = document.createElement('OPTION');
        option.setAttribute('value', plan.id);
        option.appendChild(document.createTextNode(plan.titre + ' ' + plan.code));
        select.appendChild(option);
    }

    prealables.appendChild(select);

    // les switchs boxes
    let input = document.createElement('INPUT');
    input.setAttribute('type', 'checkbox');
    input.classList.add('form-check-input');
    input.setAttribute('role', 'switch');
    input.id = 'absolu' + nombre;
    input.setAttribute('name', 'absolu[]')

    let label = document.createElement('LABEL');
    label.classList.add('form-check-label');
    label.setAttribute('for', 'absolu');
    label.appendChild(document.createTextNode('Absolu'))

    let div_input = document.createElement('DIV');
    div_input.classList.add('form-check');
    div_input.classList.add('form-switch');
    div_input.appendChild(input);
    div_input.appendChild(label);

    if(nombre<6){
        div.appendChild(prealables);
        div.appendChild(div_input);
    }
}

/**
 * La function quand on click sur le petit plus sur la section cours
 */
cours.onclick = function () {
    let div = document.getElementById('cours_select')
    let select = document.createElement('SELECT');
    select.classList.add("form-select")
    // aller trouver quelle va etre le id du select et de la switch
    let nombreEnfant = div.childElementCount + 1;
    select.id = 'cours' + nombreEnfant;
    select.setAttribute('name', 'cours[]');

    //option null
    let option;
    option = document.createElement('OPTION');
    option.setAttribute('value', null);
    option.appendChild(document.createTextNode('---'));
    select.appendChild(option);

    //mettre toutes les options
    for(const plan of plancadres){
        option = document.createElement('OPTION');
        option.setAttribute('value', plan.id);
        option.appendChild(document.createTextNode(plan.titre + ' ' + plan.code));
        select.appendChild(option);
    }
    if(nombreEnfant<6){
        div.appendChild(select)
    }
}

/**
 * La function quand on click sur le petit plus sur la section prealable
 */
competence.onclick = function () {
    let div = document.getElementById('competence_select')
    let select = document.createElement('SELECT');
    select.classList.add("form-select")
    // aller trouver quelle va etre le id du select et de la switch
    let nombreEnfant = div.childElementCount;
    let nombre = nombreEnfant/2 + 1;
    select.id = 'competence' + nombre;
    select.setAttribute('name', 'competence[]');

    //option null
    let option;
    option = document.createElement('OPTION');
    option.setAttribute('value', null);
    option.appendChild(document.createTextNode('---'));
    select.appendChild(option);

    //mettre toutes les options
    for(const comp of competences){
        option = document.createElement('OPTION');
        option.setAttribute('value', comp.id);
        option.appendChild(document.createTextNode(comp.titre + ' ' + comp.code));
        select.appendChild(option);
    }

    let input = document.createElement('INPUT');
    input.setAttribute('type', 'checkbox');
    input.classList.add('form-check-input');
    input.setAttribute('role', 'switch');
    input.id = 'complete' + nombre;
    input.setAttribute('name', 'complete[]')

    let label = document.createElement('LABEL');
    label.classList.add('form-check-label');
    label.setAttribute('for', 'complete');
    label.appendChild(document.createTextNode('Complete'))

    let div_input = document.createElement('DIV');
    div_input.classList.add('form-check');
    div_input.classList.add('form-switch');
    div_input.appendChild(input);
    div_input.appendChild(label);

    if(nombre<11){
        div.appendChild(select);
        div.appendChild(div_input);
    }
}
