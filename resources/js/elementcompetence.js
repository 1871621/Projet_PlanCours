const critereperformance = document.getElementById('critereperformance_plus');

/**
 * La function quand on click sur le petit plus sur la section prealable
 */
critereperformance.onclick = function () {
    let div = document.getElementById('critereperformance_select')
    let select = document.createElement('SELECT');
    select.classList.add("form-select")
    // aller trouver quelle va etre le id du select et de la switch
    let nombreEnfant = div.childElementCount;
    let nombre = nombreEnfant + 1;
    select.id = 'critereperformance' + nombre;

    //option null
    let option;
    option = document.createElement('OPTION');
    option.setAttribute('value', null);
    option.appendChild(document.createTextNode('---'));
    select.appendChild(option);

    //mettre toutes les options
    for(const crit of critereperformances){
        option = document.createElement('OPTION');
        option.setAttribute('value', crit.id);
        option.appendChild(document.createTextNode(crit.no + ' ' + crit.texte));
        select.appendChild(option);
    }

    if(nombre<6){
        div.appendChild(select);
    }
}
